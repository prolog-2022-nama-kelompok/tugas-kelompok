:- dynamic key/1.
:- dynamic key/2.
:- dynamic guessed/1.
:- dynamic point/1.
:- dynamic total_point/1.
:- dynamic round/1.
:- dynamic hints/1.
:- dynamic script_settings/6.
:- dynamic used_file/1.


% settings: Round, NumGuess, NumHint, InitialPoint, Pengurangan Salah, Pengurangan Hint
script_settings(3, 7, 3, -1, 1, 1).

% facts
total_point(0).
round(0).

% base case dari mencetak tebakan
print_guess([], [], _) :- !.

% jika karakter tebakan sama dengan kunci jawaban pada index yang sama,
% cetak karakter dan tambahkan index ke predikat guessed/1 
print_guess([Char|GuessTail], [Char|KeyTail], Index) :-
    !,
    Index_ is Index + 1,
    assertz(guessed(Index)),
    write(Char),
    print_guess(GuessTail, KeyTail, Index_).

% jika karakter tidak sama cek apakah ada index ada di guessed/1.
% jika ada di guessed/1 cetak karakter key. 
print_guess([_|GuessTail], [KeyChar|KeyTail], Index) :-
    guessed(Index),
    !,
    write(KeyChar),
    Index_ is Index + 1,
    print_guess(GuessTail, KeyTail, Index_).

% jika karakter tidak sama dan tidak ada di guessed
% cetak '_' ke terminal
print_guess([_|GuessTail], [_|KeyTail], Index) :-
    write('_'),
    Index_ is Index + 1,
    print_guess(GuessTail, KeyTail, Index_).

% cek apakah tebakan sama dengan kunci jawaban
correct(Guess) :- 
    string_lower(Guess, LowerGuess),
    atom_string(AtomGuess, LowerGuess),
    key(AtomGuess).

% cetak hint 
print_hint(Index) :- 
    script_settings(_, _, _, _, _, HintReduction),
    hints(Hints),
    nth0(Index, Hints, Hint),
    !,
    write("\nHint "),
    write(Index),
    write(": "),
    write(Hint),

    % kurangi poin
    point(CurrentPoint),
    retract(point(CurrentPoint)),
    CurrentPoint_ is CurrentPoint - HintReduction,
    assert(point(CurrentPoint_)).

% jika tidak ada hint lagi dari file
print_hint(_) :- 
    write("\nTidak ada hint tambahan dari file hint.").

% akan dijalankan jika masih ada hint yang boleh digunakan
% akan menambahkan hitungan hint, akan memanggil predikat print_hint
askhint(NumGuess, NumHint) :-
    script_settings(_, _, MaxHint, _, _, _),
    NumHint < MaxHint,
    !,
    NumHint_ is NumHint+1,
    print_hint(NumHint_),
    program(NumGuess, NumHint_).

% jika hitungan hint sama dengan jumlah maksimal hint yang diperbolehkan
% jalankan kode dibawah, memberi tahu pemain bahwa ia tidak bisa menggunakan hint
askhint(NumGuess, MaxHint) :-
    script_settings(_, _, MaxHint, _, _, _),
    write("\nAnda tidak bisa meminta hint lagi. Maksimal "),
    write(MaxHint),
    write(" sudah dipakai."),
    program(NumGuess, MaxHint).

% kode di bawah dijalankan ketika kata yang ditebak sama dengan kunci jawaban
process_guess(GuessString, CharList) :-
    string_chars(GuessString, CharList),
    key(KeyChar, _),
    length(CharList, Len),
    length(KeyChar, Len),
    !,
    write("Berikut karakter yang sudah berhasil ditebak: "),
    print_guess(CharList, KeyChar, 0).

% kode di bawah dijalankan ketika panjang tebakan tidak sama dengan panjang kunci jawaban
process_guess(_, _) :- key(_, Num),
    write("Panjang kata yang diberikan tidak sama dengan "),
    write(Num),
    write(".").

% proses input ketika jawaban benar
process(_, _, Guess) :- correct(Guess), !,
    point(CurrentPoint),
    total_point(Point),
    retract(total_point(Point)),
    Point_ is Point+CurrentPoint,
    assert(total_point(Point_)),
    write("\nJawaban yang anda berikan sudah benar."),
    write("\nPoint anda untuk ronde ini: "),
    write(CurrentPoint),
	write("\nTotal point anda sampai dengan ronde ini: "),
	write(Point_).

% proses input ketika jawaban salah
% kurangi poin sesuai settings pengurangan poin
process(NumGuess, NumHint, Guess) :-
    script_settings(_, MaxGuess, _, _, WrongReduction, _),
    write("\nAnda sudah menebak salah sebanyak "),
    NumGuess_ is NumGuess+1,
    write(NumGuess_),
    write(" kali dari "),
    write(MaxGuess),
    write(" percobaan yang diperbolehkan.\n"),
    process_guess(Guess, _),

    % reduce point
    point(CurrentPoint),
    retract(point(CurrentPoint)),
    CurrentPoint_ is CurrentPoint - WrongReduction,
    assert(point(CurrentPoint_)),
    program(NumGuess_, NumHint).

get_user_input(Input) :-
    read_line_to_string(user_input,X),
    sanitize_input(X, Input).

sanitize_input(Input, Sanitized) :-
    split_string(Input, "", "\s\t\n", [K|_]),
    string_lower(K, Sanitized).

% kode di bawah dijalankan ketika pemain masih belum mencapai percobaan maksimal
program(NumGuess, NumHint) :-
    script_settings(_, MaxGuess, _, _, _, _),
    NumGuess < MaxGuess,
    !,
    choice(NumGuess, NumHint).

% kode di bawah dijalankan ketika pemain sudah mencapai percobaan maksimal
program(MaxGuess, _) :-
    script_settings(_, MaxGuess, _, _, _, _),
    write("\nSayang sekali anda tidak berhasil menebak kata yang diberikan.\nJawaban yang benar adalah "),
    key(Key),
    write(Key),
    write("."),
	write("\nPoint anda untuk ronde ini: 0"),
	total_point(Total),
	write("\nTotal poin anda sampai ronde ini: "),
	write(Total).

init:-
    round(X),
    X_ is X+1,
    retract(round(X)),
    assert(round(X_)),
    generate_file_number(FileNumber),
    file_input_kata(FileNumber),
    write("\nRonde "),
    write(X_),
    write(":"),
    script_settings(_, _, _, Point, _, _),
    assert(point(Point)),
    key(_, WordLength), write("\nKata berjumlah "),
    write(WordLength), write(" karakter."),

    % ambil hint
    hints(Hints),
    nth0(0, Hints, Hint),
    write("\nHint: "),
    write(Hint).

% menghasilkan nama random untuk file yang akan dibaca sebagai hint atau soal
generate_file_number(FileNumber):-
    script_settings(MaxRound, _, _, _, _, _),
    random_between(1, MaxRound, FileNumber),
    findall(Angka, used_file(Angka), Ws),
    \+member(FileNumber, Ws),
    !,
    assertz(used_file(FileNumber)).

generate_file_number(FileNumber):-
    generate_file_number(FileNumber).

% membaca input file untuk kata misteri setiap round beserta hint nya
% file input untuk kata memiliki format <round_berapa>.txt
% file input untuk hint memiliki format <round_berapa>_hint.txt
file_input_kata(Round):-
    % add kata
    number_string(Round, StringRound),
    string_concat(StringRound, ".txt", FileInput),
    read_file_to_string(FileInput, Input_string, []),
    atom_string(AtomInput, Input_string),
    atom_chars(AtomInput, ListKarakter),
    length(ListKarakter, Length),
    assert(key(AtomInput)),
    assert(key(ListKarakter,Length)),

    % tambahkan hint dari file ke database
    read_hint_file(StringRound).

read_hint_file(StringRound) :-
    abolish(hints/1),
    string_concat(StringRound, "_hint.txt", HintFile),
    read_file_to_string(HintFile, HintInput, []),
    !,
    split_string(HintInput, "\n", "\s\t", HintList),
    assert(hints(HintList)).


read_hint_file(_) :-
    assert(hints([""])).

askguess(NumGuess, NumHint) :-
    write("\nTulis tebakan anda:\n"),
    get_user_input(Input),
    !,
    process(NumGuess, NumHint, Input).

% Pilihan pemain apakah ingin meminta hint atau memberikan tebakan kata
% terdapat goals input = 'H' untuk membedakan pilihan pemain
% jika input == 'H' akan menjalankan predikat askhint/2
% jika input \= 'H' akan menyebabkan fail dan prolog akan melakukan matching predikat choice/2 yang lain
% menggunakan cut agar hanya terdapat satu jawaban yang benar, yaitu memilih hint atau memilih memberikan tebakan
choice(NumGuess, NumHint) :-
    write("\nKetik 'H' untuk meminta hint, klik enter untuk menebak: "),
    read_line_to_string(user_input, "H"),
    !,
    askhint(NumGuess, NumHint).

% case ketika pemain memilih memberikan tebakan
choice(NumGuess, NumHint) :-
    askguess(NumGuess, NumHint).

% predikat untuk mengecek apakah terdapat next round
cek_round:-
    script_settings(MaxRound, _, _, _, _, _),
    \+round(MaxRound).

% run the whole script
start :-
    greet,
    run.

% greetings
greet :-
    % db op
    script_settings(MaxRound, NumGuess, NumHint, _, WrongReduction, HintReduction),
    InitialPoint is (NumGuess*WrongReduction) + (NumHint*HintReduction),
    retractall(script_settings(MaxRound, NumGuess, NumHint, _, WrongReduction, HintReduction)),
    asserta(script_settings(MaxRound, NumGuess, NumHint, InitialPoint, WrongReduction, HintReduction)),

    write("Selamat datang di permainan tebak kata!"),
    write("\nPermainan ini terdiri dari "),
    write(MaxRound),
    write(" ronde.\nTiap round pemain akan diberikan "),
    write(NumGuess),
    write(" kali kesempatan untuk menjawab dan "),
    write(NumHint),
    write(" kali meminta hint.\nAkan ada "),
    write(InitialPoint),
    write(" poin yang diberikan tiap awal round."),
    write("\nPoin yang didapatkan tiap round akan diakumulasikan menjadi poin total."),
    write("\nPemain bisa memilih untuk keluar saat round selesai walaupun belum round terakhir."),
    write("\n\nATURAN TAMBAHAN:"),
    write("\nPengurangan poin ketika salah menebak: "),
    write(WrongReduction),
    write("\nPengurangan poin ketika menggunakan hint: "),
    write(HintReduction).


run :- 
    init, 
    program(0, 0),
    cek_round,
	!,
    run_2.

run :-
    end2.

% akhir dari round, menghapus predikat dynamic yang ditambahkan pada round tersebut, seperti predikat point/1, guessed/1, key/1, dan key/2.
% berfugsi agar data round sebelumnya tidak tercampur dengan round selanjutnya (reset).
% menanyakan user apakah ingin mengakhiri atau melanjutkan permainan
% case ketika pemain ingin melanjutkan permainan, yaitu input == 'Y'.
run_2:-
    write("\napakah anda ingin lanjut bermain?\nketik 'Y' jika ingin melanjutkan, tekan enter untuk keluar: "),
    read_line_to_string(user_input, "Y"),
    !,
    retractall(point(_)),
    retractall(guessed(_)),
    retractall(key(_)),
    retractall(key(_,_)),
    run.

% case ketika pemain ingin mengakhiri permainan
run_2:-
    end.


end2:-
    write("\n\nRound sudah habis"),
    end.

end:-
    total_point(Point),
    write("\nTotal skor anda adalah "),
    write(Point),
    write("\nTerima kasih sudah bermain !"),
    retractall(key(_)),
    retractall(key(_,_)),
    retractall(round(_)),
    retractall(used_file(_)),
    assert(round(0)),
    retractall(total_point(_)),
    assert(total_point(0)),
    retractall(point(_)).