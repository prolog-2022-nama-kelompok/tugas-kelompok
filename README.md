# greet_and_run
Aplikasi ini dibuat untuk mensimulasikan permainan tebak kata. Terdapat sebuah kata misteri yang harus ditebak oleh pemain. Setiap turn bermain pemain boleh meminta _hint_ atau memasukkan tebakan kata. Untuk dapat melakukan hal tersebut, aplikasi pertama harus dapat mengidentifikasi _input_ dari pemain, apakah meminta _hint_ atau memasukkan tebakan kata. Untuk setiap _turn_, aplikasi harus dapat menyimpan _progress_ dari pemain agar, diujung permainan aplikasi bisa menyimpulkan hasil permainan dari pengguna.

Tantangan yang ditemui pada hal tersebut adalah bagaimana aplikasi dapat melakukan pengecekan _input_ dari pengguna apakah sama dengan kata misteri di dalam aplikasi. Selain itu, aplikasi kami dapat berjalan beberapa kali karena permainan bersifat _multiple round._ Karena aplikasi kami multiple-round kami perlu menghitung dan menyimpan poin dari setiap round yang diperoleh pemain.

Aplikasi ini ditujukan untuk orang yang senang bermain tebak kata. Belakangan ini, kami melihat antusiasme orang-orang memainkan aplikasi wordle, yang secara umum merupakan aplikasi permainan tebak kata. Selain itu, aplikasi ini dapat digunakan oleh orang-orang untuk mengasah pengetahuan mereka terhadap kata-kata dalam bahasa indonesia.

**Requirements** : [SWI-Prolog 8.4.2-1](https://www.swi-prolog.org/download/stable)
